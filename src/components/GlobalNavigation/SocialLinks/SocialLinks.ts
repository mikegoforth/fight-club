import Vue from "vue";
import { Component } from "vue-property-decorator";

@Component({
  name: "fc-social-links"
})
export default class SocialLinks extends Vue {
  // Public Properties
  public isStreaming = false;
  public isOnAir = false;
  public hasRaiderIoEnabled = true;
  public hasDiscordEnabled = true;
}

Vue.component("fc-social-links", SocialLinks);
