import Vue from "vue";
import { Component } from "vue-property-decorator";
import Branding from "./Branding/Branding.vue";
import NavigationLinks from "./NavigationLinks/NavigationLinks.vue";
import QuoteBoard from "./QuoteBoard/QuoteBoard.vue";
import SocialLinks from "./SocialLinks/SocialLinks.vue";

@Component({
  name: "fc-global-navigation",
  components: {
    Branding,
    NavigationLinks,
    QuoteBoard,
    SocialLinks
  }
})
export default class GlobalNavigation extends Vue {}

Vue.component("fc-global-navigation", GlobalNavigation);
