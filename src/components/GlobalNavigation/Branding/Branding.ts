import Vue from "vue";
import { Component } from "vue-property-decorator";

@Component({
  name: "fc-branding"
})
export default class Branding extends Vue {}

Vue.component("fc-branding", Branding);
