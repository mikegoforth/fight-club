import Vue from "vue";
import { Component } from "vue-property-decorator";

@Component({
  name: "fc-quote-board"
})
export default class QuoteBoard extends Vue {
  generatedQuote = "";
  messages: Array<string> = [
    "Back with another banger! ~ Auresk",
    "Stay strapped or get clapped. ~ Auresk"
  ];

  getRandomInt(max: number) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  generateQuote(generatedQuote: string) {
    const i = this.getRandomInt(this.messages.length);
    generatedQuote = this.messages[i];
    return generatedQuote;
  }
}

Vue.component("fc-quote-board", QuoteBoard);
