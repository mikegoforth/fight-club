import Vue from "vue";
import { Component } from "vue-property-decorator";

@Component({
  name: "fc-nav-links"
})
export default class NavigationLinks extends Vue {}

Vue.component("fc-nav-links", NavigationLinks);
